import { createApp } from 'vue';
import Maska from 'maska';
// import vuetify from 'vuetify';

import ApiService from './services/api';
import router from './router/index';
import { URLS_LIST } from './router/constants';
import store from './store';
import App from './App.vue';

import InputText 			from './components/forms/InputText';
import InputEmail 		from './components/forms/InputEmail';
import InputPassword 	from './components/forms/InputPassword';
import InputArea 			from './components/forms/InputArea';
import InputTextarea 			from './components/forms/InputTextarea';
import InputCheckbox 	from './components/forms/InputCheckbox';
import FMultiSelect 	from './components/forms/MultiSelect';
import SingleSelect 	from './components/forms/SingleSelect';
import BtnSubmit 			from './components/forms/BtnSubmit';


import 'vue-advanced-cropper/dist/style.css';

import './assets/scss/multiselect.scss'

import "./assets/scss/material.scss";

import "./assets/scss/main.scss";

import VueMaterialAdapter from 'vue-material-adapter';
//https://pgbross.github.io/vue-material-adapter/#/
//https://github.com/vueform/multiselect

const app = createApp(App);
ApiService.init(app);
store.dispatch('auth/checkAuth');
app.use(Maska);
app.use(router);
app.use(VueMaterialAdapter);

// todo check
app.use(store);
app.config.globalProperties.URLS_LIST = URLS_LIST;
app.component('input-text', InputText);
app.component('select-many', FMultiSelect);
app.component('select-one', SingleSelect);
app.component('input-email', InputEmail);
app.component('input-password', InputPassword);
app.component('input-area', InputArea);
app.component('input-textarea', InputTextarea);
app.component('input-checkbox', InputCheckbox);
app.component('btn-submit', BtnSubmit);
//store.dispatch('auth/checkAuth')
app.mount('#fan');
