export const updateErrors = (validationObject, errorsObject) => {
    //remove old
    Object.keys(validationObject).forEach((key) => {
        if(typeof errorsObject[key] === "undefined") {
            delete validationObject[key];
        }
    });
    //set new
    Object.keys(errorsObject).forEach((key) => {
        validationObject[key] = errorsObject[key][0];
    }); 
}

export const isArraysEqual = (a, b) => { //console.log(typeof a, typeof b); return true;
    if(a.length !== b.length) return false;
    return a.every((val, i) => val == b[i]);
}