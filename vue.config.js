
const path = require('path');

let wConfig = {
    css: {
      loaderOptions: {
        sass: {
          sassOptions: {
            includePaths: [path.resolve(__dirname,'node_modules')]
          }
        }
    }
}
  
};

if (process.env.NODE_ENV === 'production') {
  wConfig.publicPath = '/';
  wConfig.outputDir = 'public_html';
  wConfig.productionSourceMap = false;
  wConfig.assetsDir = 'assets';
  wConfig.filenameHashing = false;
  wConfig.configureWebpack = {
    //optimization: {
      //splitChunks: true
    //}
    
    optimization: {
      splitChunks: {
           cacheGroups: {
            vendors: {
              name: `all-vendors`,
              test: /[\\/]node_modules[\\/]/,
              priority: -10,
              chunks: 'initial'
            },
            common: {
              name: `chunk-common`,
              minChunks: 2,
              priority: -20,
              chunks: 'initial',
              reuseExistingChunk: true
            }
          }  
        
      }
    }
  };
 
} else if(process.env.NODE_ENV === 'development'){
   /* if(process.env.VUE_PRROXY_APP_APIURL)  {
        wConfig.devServer = {
          proxy: process.env.VUE_PRROXY_APP_APIURL
        }
    } */
}

module.exports =  wConfig;
/*
cacheGroups: {
            vendors: {
              name: `chunk-vendors`,
              test: /[\\/]node_modules[\\/]/,
              priority: -10,
              chunks: 'initial'
            },
            common: {
              name: `chunk-common`,
              minChunks: 2,
              priority: -20,
              chunks: 'initial',
              reuseExistingChunk: true
            }
          }     */