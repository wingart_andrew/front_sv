// Auth module: all information about logged user
import ApiService from '@/services/api';
import { defaultAvatar } from '@/static-data/user.js';

const auth = {
    namespaced: true,
    state: () => ({ 
        isAuth: false,
        userData: null,
        isLoaded: false,
        avatar: defaultAvatar,
        displayName: null,
    }),
    mutations: {
        setToken(state, payload) {
            let token, refresh;
            if( typeof payload === 'string') {
                token = payload;
            } else if(payload.access_token){
                token = payload.access_token;
                refresh = payload.refresh_token;
            }
            ApiService.updateToken(token, refresh);
            if((! state.userAuth) && token ) {
                state.userAuth = true;               
            }
        },        
        purgeAuth(state) {
            ApiService.removeToken();
            state.isAuth = false; 
            state.isLoaded = true;
            state.userData = null;
        }, 
        setUser(state, data)   {
            state.userData = data;
            if(data.thumbnail && data.thumbnail.url) {
                state.avatar = data.thumbnail.url;
            } else {
                state.avatar = defaultAvatar;
            } 
            state.displayName = state.userData.full_name; 
            state.isLoaded = true;
            state.isAuth = true; 
        },    
        updateUser(state, data) { 
            if(! state.userData) {
                state.userData = {};
            }
            Object.assign(state.userData, data);
            if(state.userData.thumbnail && state.userData.thumbnail.url) {
                state.avatar = state.userData.thumbnail.url;
            } else {
                state.avatar = defaultAvatar;
            } 
            state.displayName = state.userData.full_name; 
            state.isLoaded = true;
        }
    },
    actions: {
        checkAuth(context) { 
            if(ApiService.getToken()) {  
                context.state.isAuth = true;
                ApiService.http().post('auth/user')
                    .then((res) =>{  console.log(res);
                        if(res.data.data) {
                            context.commit('setUser', res.data.data);
                        } else {
                            context.commit('purgeAuth');
                        }
                    })
                    .catch((err) => {
                        context.commit('purgeAuth');
                    })
            } else { 
                context.commit('purgeAuth');                
            }
        },
        forceUpdate(context) {
            if(ApiService.getToken()) {  
                ApiService.http().post('auth/user')
                .then((res) =>{  
                    if(res.data.data) {
                        context.commit('setUser', res.data.data);
                    }
                })
                .catch((err) => {
                    //context.commit('purgeAuth');
                })                    
            } 
        },
        logout(context) {
            ApiService.http().post('auth/logout')
            .then((res) =>{  
                console.log(res);
                context.commit('purgeAuth'); 
            })
            
        }
    },    
  }

export default auth;