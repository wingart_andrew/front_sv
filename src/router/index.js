import { createRouter, createWebHistory } from 'vue-router';
import HomePage from '@/views/Home';
import Page404 from '@/views/404';

import { URLS_LIST, NESTED_URLS } from './constants.js';

const routes = [
    { path: URLS_LIST.HOME, component: HomePage},
    //{ path: URLS_LIST.DASHBOARD , component: () => import('@/views/dashboard/main') },
    // { path: URLS_LIST.DASHBOARD_ARTICLES, component: () => import('@/views/DashboardArticles') },
    // { path: URLS_LIST.DASHBOARD_ARTICLE, component: () => import('@/views/DashboardArticle') },
    //accounts
    {
      path: URLS_LIST.ACCOUNT_LOGIN,
      component: () => import('@/views/account/login'),
      meta: { emptyLayout: true }
    },
    {
      path: URLS_LIST.ACCOUNT_SIGNUP,
      component: () => import('@/views/account/signup/index'),
      meta: { emptyLayout: true }
    },
    {
      path: URLS_LIST.ACCOUNT_RESET_PASSWORD,
      component: () => import('@/views/account/password-reset/index'),
      meta: { emptyLayout: true }
    },
    //dashboard
    {
      path: NESTED_URLS.DASHBOARD.INDEX ,
      component: () => import('@/views/dashboard/index'),
      children: [
        {
          path:  NESTED_URLS.DASHBOARD.MAIN,
          component: () => import('@/views/dashboard/main/index'),
          meta: { nav: 'main' }
        },
        {
          path:  NESTED_URLS.DASHBOARD.PLAYERS,
          component: () => import('@/views/dashboard/players/index'),
          meta: { nav: 'player' }
        },
        {
          path:  NESTED_URLS.DASHBOARD.GAMES,
          component: () => import('@/views/dashboard/games/index'),
          meta: { nav: 'game' }
        },
        {
          path:  NESTED_URLS.DASHBOARD.TEAMS,
          component: () => import('@/views/dashboard/teams/index'),
          meta: { nav: 'team' }
        },
        {
          path:  NESTED_URLS.DASHBOARD.STADIUMS,
          component: () => import('@/views/dashboard/stadiums/index'),
          meta: { nav: 'stadium' }
        },
        {
          path:  NESTED_URLS.DASHBOARD.ARTICLES,
          component: () => import('@/views/dashboard/articles/index'),
          meta: { nav: 'article' }
        },
        {
          path:  NESTED_URLS.DASHBOARD.SETTINGS,
          component: () => import('@/views/dashboard/settings/index'),
          meta: { nav: 'setting' }
        },
      ]
    },
    //players
    { path: URLS_LIST.PLAYERS, component: () => import('@/views/Players') },
    {
      path: NESTED_URLS.PLAYER.INDEX ,
      component: () => import('@/views/player/index'),
      children: [
        {
          path:  NESTED_URLS.PLAYER.MAIN,
          component: () => import('@/views/player/summary/index'),
          name: URLS_LIST.PLAYER_SUMMARY,
          meta: { nav: 'summary' }
        },
        {
          path:  NESTED_URLS.PLAYER.REPORTS,
          component: () => import('@/views/player/reports/index'),
          name: URLS_LIST.PLAYER_REPORTS,
          meta: { nav: 'reports' }
        },
        {
          path:  NESTED_URLS.PLAYER.REPORT,
          component: () => import('@/views/player/report/index'),
          name: URLS_LIST.PLAYER_REPORT,
          meta: { nav: 'reports' }
        },
        {
          path:  NESTED_URLS.PLAYER.GAME,
          component: () => import('@/views/player/game/index'),
          name: URLS_LIST.PLAYER_GAME,
          meta: { nav: 'game' }
        },
        {
          path:  NESTED_URLS.PLAYER.ADD_REPORT,
          component: () => import('@/views/player/add-report/index'),
          name: URLS_LIST.PLAYER_ADD_REPORT,
          meta: { nav: 'summary', asideBanner: true }
        },
      ]
    },

   // { path: '/', component: HomePage},
    { path: '/player', component: () => import('@/views/Player') },
    { path: '/player-reports', component: () => import('@/views/PlayerReports') },
    { path: '/', component: HomePage},
    { path: '/teams', component: () => import('@/views/Teams') },
    //{ path: '/players', component: () => import('@/views/Players') },
    { path: '/stadiums', component: () => import('@/views/Stadiums') },
    { path: '/games', component: () => import('@/views/Games') },
    { path: '/player', component: () => import('@/views/Player') },
    { path: '/player-reports', component: () => import('@/views/PlayerReports') },
    { path: '/player-data', component: () => import('@/views/PlayerData') },
    { path: '/compare', component: () => import('@/views/Compare') },
    { path: '/compare-team', component: () => import('@/views/CompareTeam') },
    { path: '/compare-stadiums', component: () => import('@/views/CompareStadiums') },


    { path: '/tester', component: () => import('@/views/Tester') },
    { path: '/:pathMatch(.*)*', component: Page404 },
    { path: '/:pathMatch(.*)', component: Page404 },
];

const scrollBehavior =  (to) => {
    if (to.hash) {
        return {
          selector: to.hash
        }
    } else {
        return { left: 0, top: 0 }
    }
  }

const history = createWebHistory();

const router = createRouter({
  history: history,
  routes: routes,
  scrollBehavior:  scrollBehavior
});
export default router;
