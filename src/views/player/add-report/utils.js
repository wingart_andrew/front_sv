export const getProxyData = () => {
    return [
    {
      name: 'pr1',
      value: '',
      comment: '',
      label: 'Power Moves:'
    },
    {
        name: 'pr2',
        value: '',
        comment: '',
        label: 'Acceleration:'
    },      
    {
        name: 'pr3',
        value: '',
        comment: '',
        label: 'Block Shedding'
    },
    {
        name: 'pr4',
        value: '',
        comment: '',
        label: 'Penetration'
      },
      {
        name: 'pr5',
        value: '',
        comment: '',
        label: 'Motor'
      },
      {
        name: 'pr6',
        value: '',
        comment: '',
        label: 'Speed'
      },        
      {
        name: 'pr7',
        value: '',
        comment: '',
        label: 'Durability'
      },
      {
        name: 'pr8',
        value: '',
        comment: '',
        label: 'Size'
      },
      {
        name: 'pr9',
        value: '',
        comment: '',
        label: 'Character'
      },
      {
        name: 'pr10',
        value: '',
        comment: '',
        label: 'Technique'
      },
      {
        name: 'pr11',
        value: '',
        comment: '',
        label: 'Intangibles'
      },
      {
        name: 'pr12',
        value: '',
        comment: '',
        label: 'Consistency'
      },
  ]
};
// Power Moves:  Acceleration:  Block Shedding  Penetration
// Motor  Speed  Durability  Size 
