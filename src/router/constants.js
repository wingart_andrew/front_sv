export const URLS_LIST = {
    HOME: '/',
    DASHBOARD: '/dashboard',
    DASHBOARD_PLAYERS: '/dashboard/players',
    DASHBOARD_GAMES: '/dashboard/games',
    DASHBOARD_TEAMS: '/dashboard/teams',
    DASHBOARD_STADIUMS: '/dashboard/stadiums',
    DASHBOARD_ARTICLES: '/dashboard/articles',
    DASHBOARD_SETTINGS: '/dashboard/settings',
    //DASHBOARD_ARTICLES: "/dashboard-articles",
    DASHBOARD_ARTICLE: "/dashboard-article",
    //account pages
    ACCOUNT: '/account',
    ACCOUNT_SIGNUP: "/account/signup",
    ACCOUNT_LOGIN: "/account/login",
    ACCOUNT_RESET_PASSWORD: "/account/reset-password",
    //players
    PLAYERS: '/players',
    //player
    PLAYER_MAIN: '/player/:player_id',
    PLAYER_SUMMARY: '/player/:player_id',
    PLAYER_REPORTS: '/player/:player_id/reports',
    PLAYER_GAME: '/player/:player_id/game',
    PLAYER_REPORT: '/player/:player_id/report/:report_id',
    PLAYER_ADD_REPORT: '/player/:player_id/add-report',
    //static
    STATIC_TERMS: "/page/terms",
};

export const NESTED_URLS = {
    DASHBOARD: {
        INDEX: '/dashboard', 
        MAIN: '',
        PLAYERS: 'players', 
        GAMES: 'games',
        TEAMS: 'teams',
        STADIUMS: 'stadiums',
        ARTICLES: 'articles',
        ARTICLE: 'article',
        SETTINGS: 'settings'
    },
    PLAYER: {
        INDEX: '/player/:player_id',
        SUMMARY: '',
        MAIN: '',
        REPORTS: 'reports',
        GAME: 'game',
        REPORT: 'report/:report_id',
        ADD_REPORT: 'add-report',

    }
}
