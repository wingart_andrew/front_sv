import axios from "axios";
import VueAxios from "vue-axios";

const ApiService = {
    token: null,
    init(app) {
       
        app.use(VueAxios, axios);
        this.token = (localStorage.getItem('fantoken')) ? localStorage.getItem('fantoken') : null;
        if( this.token) {
            axios.defaults.headers.common['Authorization'] = `Bearer ${ this.token }`;
        }  
        axios.defaults.baseURL = process.env.VUE_APP_APIURL + '/v1/';      
        axios.defaults.headers['Accept'] = 'application/json';
        axios.defaults.headers['Content-Type'] = 'application/json';         
        axios.interceptors.response.use(function (response) {
            return response;
        }, function (error) {    
            //console.log('axios error', error);
            return Promise.reject(error); 
              
        });
    },  
    setHeader(head, value) {
        axios.defaults.headers[head] = value; 
    },
    removeHeader(head) {
        delete axios.defaults.headers[head]; 
    },
    getToken() {
        const token = localStorage.getItem('fantoken'); 
        return token;
    },
    updateToken( token, refreshToken) {
        this.token = token;
        localStorage.setItem('fantoken', token);
        if(refreshToken) {
            localStorage.setItem('fanrefresh', refreshToken);
        }
       
        axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
    },
    removeToken() {
        this.token = null;
        localStorage.removeItem('fantoken');
        localStorage.removeItem('fanrefresh');
        delete axios.defaults.headers.common['Authorization'];        
    },
    http() {
        return axios;
    }

};
export default ApiService;