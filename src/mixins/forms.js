/* 
forms :{
    formName: {
        validation: {},
        isDisabled: false
        //optionally (proxy)
    }
}

*/

import { updateErrors } from '@/utils/forms';

const formsMixin = {
    data() {
        return {
            forms: {                               
            }
        };
    },
    methods: {
        fieldResetError(target) { 
            const fieldName = target.name;
            const formName = target.form.name; 
            if((! fieldName) || (! formName)) return;
            if(! this.forms[formName]) return;
            if(! this.forms[formName].vaildation) return;
            if(this.forms[formName].vaildation[fieldName]) {
                delete this.forms[formName].vaildation[fieldName];
            } else if(this.forms[formName].proxyKeys && this.forms[formName].proxyKeys[fieldName])  {
                this.forms[formName].proxyKeys[fieldName].forEach((key) => {
                    if(this.forms[formName].vaildation[key]) {
                        delete this.forms[formName].vaildation[key];
                    }
                });
            }         
        },
        insertFormErrors(formName, response) { 
            if((! response) || (! this.forms[formName])) return;            
            if(response.status === 422) { 
                updateErrors(this.forms[formName].vaildation, response.data.errors);
            }
        },
        extractIds(obj, key = 'id') {
            let result = [];
            if(! Array.isArray(obj)) return result;
            obj.forEach((item) => {
                if(item[key]) {
                    result.push(item[key]);
                }
            })
            return result;
        },
        getDouble(obj) {
            return Object.assign({}, obj);
        },
        extractId(obj, key = 'id') {
            if(obj[key]) return obj[key];
            return obj;
        }
    }
}
export default formsMixin;